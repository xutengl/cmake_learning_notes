#include <iostream>
#include "config.h"

using namespace std;

int main(int argc, char* argv[]) {
    cout << "PROJECT_NAME: " << PROJECT_NAME << endl;
    cout << "PROJECT_VERSION: " << PROJECT_VERSION << endl;
    cout << "VERSION_MAJOR: " << VERSION_MAJOR << endl;
    cout << "VERSION_MINOR: " << VERSION_MINOR << endl;
    cout << "VERSION_PATCH: " << VERSION_PATCH << endl;
    cout << "VERSION_TWEAK: " << VERSION_TWEAK << endl;
    cout << "MYVAR: " << MYVAR << endl;

    return 0;
}